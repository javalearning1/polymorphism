class Bird{
    public void sing(){
        System.out.println("coo coo coo");
    }
}
class Parrot extends Bird{
    public void sing(){
        System.out.println("kee kee kee");
    }
}

public class Polymorphism {
    public static void main(String[] args) {
        Parrot p =new Parrot();
        p.sing();

    }

}


